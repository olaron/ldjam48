wallmap = {}
wallbackmap = {}
backgroundmap = {}
walltilemap = {}

QUAD_SIZE = 4
PIXEL_SIZE = 8

WALL_WIDTH = 25
WALL_HEIGHT = 2000

BLOOD_Y = 700
METAL_Y = 850
ROBOT_Y = 950
END_Y = 1100

function randomLightColor()
    local r = love.math.random()
    if r < 0.4 then
        return {{0.5,0.5,1,1},{0.1,0.1,0.5,1}}
    end
    if r < 0.7 then
        return {{0.5,1,0.5,1},{0.1,0.5,0.1,1}}
    end
    return {{1,0.5,0.5,1},{0.5,0.1,0.1,1}}
end

function initWallMap()
    WALL_WIDTH = math.ceil(SCREEN_WIDTH/(QUAD_SIZE*PIXEL_SIZE))
    for x = 0, WALL_WIDTH, 1 do
        wallmap[x] = {}
        walltilemap[x] = {}
        backgroundmap[x] = {}
        wallbackmap[x] = {}
    end

    for y = -1, WALL_HEIGHT/2, 1 do
        for x = 0, WALL_WIDTH, 1 do
            backgroundmap[x][y] = {0}
            if ROBOT_Y/2 <= y then
                local p = clamp(lerp(1,0.95,(y-ROBOT_Y/2)/50),0.95,1)
                if love.math.random() > p then
                    backgroundmap[x][y] = {1,randomLightColor(),0}
                end
            end
        end
    end
    for y = -1, WALL_HEIGHT, 1 do
        for x = 0, WALL_WIDTH, 1 do
            local w = 0
            
            local p = clamp(lerp(0.1,0.6,y/BLOOD_Y),0.1,0.6)
            if y > BLOOD_Y then
                p = clamp(lerp(0.6,0.3,(y-BLOOD_Y)/(METAL_Y-BLOOD_Y)),0.3,0.6)
            end
            if y > METAL_Y then
                if math.floor(x/2) % 2 == 0 or math.floor(y/2) % 10 == 0 then
                    p = p * clamp(lerp(1,0.3,(y-METAL_Y)/100),0.3,1)
                else
                    p = p * clamp(lerp(1,3,(y-METAL_Y)/100),1,3)
                    p = clamp(p,0,0.9)
                end
            end
            if love.math.random() > p then
                w = 1
            end
            wallmap[x][y] = w

            if METAL_Y - 100 < y and y < METAL_Y then
                local pt =  1 - (y - (METAL_Y - 100))/100
                if love.math.random() > pt then
                    walltilemap[x][y] = 1
                else
                    walltilemap[x][y] = 0
                end
            elseif METAL_Y <= y then
                walltilemap[x][y] = 1
            else
                walltilemap[x][y] = 0
            end
        end
    end
    for y = -1, WALL_HEIGHT, 1 do
        for x = 0, WALL_WIDTH, 1 do
            local w = 0
            
            local p = clamp(lerp(0.1,0.6,y/BLOOD_Y),0.1,0.6)
            if y > BLOOD_Y then
                p = clamp(lerp(0.6,0.3,(y-BLOOD_Y)/(METAL_Y-BLOOD_Y)),0.3,0.6)
            end
            if y > METAL_Y then
                if math.floor(x/2) % 2 == 1 or math.floor(y/2) % 10 == 0 then
                    p = p * clamp(lerp(1,0.3,(y-METAL_Y)/100),0.3,1)
                else
                    p = p * clamp(lerp(1,3,(y-METAL_Y)/100),1,3)
                    p = clamp(p,0,0.9)
                end
            end
            if love.math.random() > p then
                w = 1
            end
            -- if y > ROBOT_Y then
            --     w = 0
            -- end
            wallbackmap[x][math.floor(y/2)] = w
        end
    end
end

function loadWallTextures()
    wallatlas = love.graphics.newImage("resources/textures/wall1.png")
    wallcracks = love.graphics.newImage("resources/textures/cracks.png")
    wallcracks:setWrap('repeat')
    local w,h = wallcracks:getDimensions()
    wallcracksquad = love.graphics.newQuad(0,0,w,h,w,h)
end



wallquads = {}
backgroundquads = {}

function initWallQuads()
    local qs = QUAD_SIZE
    wallquads[0] = love.graphics.newQuad(qs, 3*qs, qs, qs, wallatlas:getDimensions())
    wallquads[1] = love.graphics.newQuad(0, 0, qs, qs, wallatlas:getDimensions())
    wallquads[2] = love.graphics.newQuad(2*qs, 0, qs, qs, wallatlas:getDimensions())
    wallquads[3] = love.graphics.newQuad(qs, 0, qs, qs, wallatlas:getDimensions())
    wallquads[4] = love.graphics.newQuad(0, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[5] = love.graphics.newQuad(0, qs, qs, qs, wallatlas:getDimensions())
    wallquads[6] = love.graphics.newQuad(7*qs, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[7] = love.graphics.newQuad(5*qs, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[8] = love.graphics.newQuad(2*qs, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[9] = love.graphics.newQuad(7*qs, qs, qs, qs, wallatlas:getDimensions())
    wallquads[10] = love.graphics.newQuad(2*qs, qs, qs, qs, wallatlas:getDimensions())
    wallquads[11] = love.graphics.newQuad(4*qs, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[12] = love.graphics.newQuad(qs, 2*qs, qs, qs, wallatlas:getDimensions())
    wallquads[13] = love.graphics.newQuad(5*qs, qs, qs, qs, wallatlas:getDimensions())
    wallquads[14] = love.graphics.newQuad(4*qs, qs, qs, qs, wallatlas:getDimensions())
    wallquads[15] = love.graphics.newQuad(qs, qs, qs, qs, wallatlas:getDimensions())

    wallquads[16] = love.graphics.newQuad(qs, 3*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[17] = love.graphics.newQuad(0, 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[18] = love.graphics.newQuad(2*qs, 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[19] = love.graphics.newQuad(qs, 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[20] = love.graphics.newQuad(0, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[21] = love.graphics.newQuad(0, qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[22] = love.graphics.newQuad(7*qs, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[23] = love.graphics.newQuad(5*qs, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[24] = love.graphics.newQuad(2*qs, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[25] = love.graphics.newQuad(7*qs, qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[26] = love.graphics.newQuad(2*qs, qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[27] = love.graphics.newQuad(4*qs, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[28] = love.graphics.newQuad(qs, 2*qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[29] = love.graphics.newQuad(5*qs, qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[30] = love.graphics.newQuad(4*qs, qs + 4*qs, qs, qs, wallatlas:getDimensions())
    wallquads[31] = love.graphics.newQuad(qs, qs + 4*qs, qs, qs, wallatlas:getDimensions())

    backgroundquads[1] = love.graphics.newQuad(9*qs, qs, qs, qs, wallatlas:getDimensions())
end

function initWall()
    loadWallTextures()
    initWallQuads()
    initWallMap()
end

previous_power = 1

function wallColor(y)
    if 0 < y and y < 500 then
        return {0.7,0.7,0.7,1}
    end
    if 500 <= y and y < BLOOD_Y then
        return {0.7,lerp(0.7,0,(y-500)/(BLOOD_Y-500)),lerp(0.7,0,(y-500)/(BLOOD_Y-500)),1}
    end
    if BLOOD_Y <= y and y < METAL_Y then
        return {0.7,lerp(0,0.7,(y-BLOOD_Y)/(METAL_Y-BLOOD_Y)),lerp(0,0.7,(y-BLOOD_Y)/(METAL_Y-BLOOD_Y)),1}
    end
    -- if 1000 <= y and y < 1500 then
    --     return {0.7,lerp(0.7,0,(y-500)/500),lerp(0.7,0,(y-500)/500),1}
    -- end
    return {0.7,0.7,0.7,1}
end

function ambiantLightColor(y)
    local l = clamp((500-y)/500,0,1)
    return {l,l,l,1}
end

function cracksColor(y)
    if y < METAL_Y - 100 then
        return {0.5,0.5,0.5,1}
    end
    if METAL_Y - 100 <= y and y < METAL_Y then
        local c = 0.5 - ((y - (METAL_Y - 100))/100)/2
        return {c,c,c,1}
    end
    return {0,0,0,1}
end

function glitchStencil()
    love.graphics.clear()
    local l = math.floor(clamp(((y-(END_Y-100))/100)*16,0,16))
    for i = 1, l, 1 do
        local x = math.floor(love.math.random(0,SCREEN_WIDTH/PIXEL_SIZE))
        local y = math.floor(love.math.random(0,SCREEN_HEIGHT/PIXEL_SIZE))
        love.graphics.rectangle("fill", x*PIXEL_SIZE, y*PIXEL_SIZE, SCREEN_WIDTH, PIXEL_SIZE)
    end
end

function drawWall(player_y,dt)
    local bg_y = player_y/2
    love.graphics.setCanvas(light)
    local new_power = love.math.randomNormal(0.05,1)
    local power = (previous_power + new_power)/2
    previous_power = new_power
    love.graphics.setBlendMode('alpha')
    love.graphics.clear(ambiantLightColor(player_y))
    love.graphics.setBlendMode('add')
    love.graphics.setColor(0.1*power,0.1*power,0)
    love.graphics.circle('fill',450,300,800)
    love.graphics.setColor(0.3*power,0.3*power,0.1*power)
    love.graphics.circle('fill',450,300,300 + 100*power)
    love.graphics.setColor(1*power,1*power,0.7*power)
    love.graphics.circle('fill',450,300,200 + 50*power)

    love.graphics.setCanvas(backgroundlight)
    love.graphics.setBlendMode('replace')
    love.graphics.clear(0.0,0.0,0.0,1)

    love.graphics.setCanvas(background)
    love.graphics.setBlendMode('replace')
    love.graphics.clear(0,0,0,0)

    love.graphics.setColor(wallColor(player_y))
    love.graphics.setBlendMode('alpha')
    for y = math.floor(bg_y), math.ceil(bg_y) + SCREEN_HEIGHT/(QUAD_SIZE*PIXEL_SIZE), 1 do
        for x = 0, WALL_WIDTH-1, 1 do
            local q = 8 * wallbackmap[x][y-1] + 4 * wallbackmap[x+1][y-1] + 2 * wallbackmap[x][y] + wallbackmap[x+1][y] + 16 * walltilemap[x][y*2]
            local quad = wallquads[q]
            love.graphics.draw(wallatlas, quad, x * (QUAD_SIZE*PIXEL_SIZE),  (y - bg_y)* (QUAD_SIZE*PIXEL_SIZE),0,PIXEL_SIZE, PIXEL_SIZE)
        end
    end
    love.graphics.setBlendMode('subtract')
    love.graphics.setColor(cracksColor(player_y))
    for y = 0, SCREEN_HEIGHT + wallcracks:getHeight()*PIXEL_SIZE, wallcracks:getHeight()*PIXEL_SIZE do
        for x = 0, SCREEN_WIDTH, wallcracks:getWidth() * PIXEL_SIZE do
            love.graphics.draw(wallcracks,wallcracksquad,x,y - (((bg_y * QUAD_SIZE) * PIXEL_SIZE) % (wallcracks:getHeight() * PIXEL_SIZE)),0,PIXEL_SIZE,PIXEL_SIZE)
        end
    end

    for y = math.floor(bg_y), math.ceil(bg_y) + SCREEN_HEIGHT/(QUAD_SIZE*PIXEL_SIZE), 1 do
        for x = 0, WALL_WIDTH-1, 1 do
            local t = backgroundmap[x][y]
            if t[1] == 1 then
                love.graphics.setColor(1,1,1,1)
                love.graphics.setBlendMode('alpha')
                love.graphics.setCanvas(background)
                love.graphics.draw(wallatlas, backgroundquads[1], x * (QUAD_SIZE*PIXEL_SIZE),  (y - bg_y)* (QUAD_SIZE*PIXEL_SIZE),0,PIXEL_SIZE, PIXEL_SIZE)
                if t[3] == 0 and love.math.random() > 0.999 then
                    t[3] = 0.2
                end
                if t[3] ~= 0 then
                    love.graphics.setColor(t[2][1])
                    love.graphics.rectangle('fill',x * (QUAD_SIZE*PIXEL_SIZE) + PIXEL_SIZE, (y - bg_y)* (QUAD_SIZE*PIXEL_SIZE) + PIXEL_SIZE,2*PIXEL_SIZE, 2*PIXEL_SIZE)
                    love.graphics.setCanvas(backgroundlight)
                    love.graphics.setBlendMode('screen')
                    love.graphics.circle('fill',x * (QUAD_SIZE*PIXEL_SIZE) + 2*PIXEL_SIZE, (y - bg_y)* (QUAD_SIZE*PIXEL_SIZE) + 2*PIXEL_SIZE,100)
                    love.graphics.setColor(t[2][2])
                    love.graphics.circle('fill',x * (QUAD_SIZE*PIXEL_SIZE) + 2*PIXEL_SIZE, (y - bg_y)* (QUAD_SIZE*PIXEL_SIZE) + 2*PIXEL_SIZE,200)
                    t[3] = t[3] - dt
                    if t[3] < 0 then
                        t[3] = 0
                    end
                end
            end
        end
    end

    love.graphics.setCanvas(backgroundlight)
    love.graphics.setColor(0.2,0.2,0.2,1)
    love.graphics.setBlendMode('screen')
    love.graphics.draw(light)

    love.graphics.setCanvas(background)
    love.graphics.setColor(1,1,1,1)
    love.graphics.setBlendMode('multiply','premultiplied')
    love.graphics.draw(backgroundlight)

    love.graphics.setCanvas(screen)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(1,1,1,1)
    love.graphics.draw(background)

    love.graphics.setCanvas(wall)
    love.graphics.setBlendMode('replace')
    love.graphics.clear(0,0,0,0)
    love.graphics.setColor(wallColor(player_y))
    love.graphics.setBlendMode('alpha')
    for y = math.floor(player_y), math.ceil(player_y) + SCREEN_HEIGHT/(QUAD_SIZE*PIXEL_SIZE), 1 do
        for x = 0, WALL_WIDTH-1, 1 do
            local q = 8 * wallmap[x][y-1] + 4 * wallmap[x+1][y-1] + 2 * wallmap[x][y] + wallmap[x+1][y] + 16 * walltilemap[x][y]
            local quad = wallquads[q]
            love.graphics.draw(wallatlas, quad, x * (QUAD_SIZE*PIXEL_SIZE),  (y - player_y)* (QUAD_SIZE*PIXEL_SIZE),0,PIXEL_SIZE, PIXEL_SIZE)
        end
    end
    love.graphics.setBlendMode('subtract')
    love.graphics.setColor(cracksColor(player_y))
    for y = 0, SCREEN_HEIGHT + wallcracks:getHeight()*PIXEL_SIZE, wallcracks:getHeight()*PIXEL_SIZE do
        for x = 0, SCREEN_WIDTH, wallcracks:getWidth() * PIXEL_SIZE do
            love.graphics.draw(wallcracks,wallcracksquad,x,y - (((player_y * QUAD_SIZE) * PIXEL_SIZE) % (wallcracks:getHeight() * PIXEL_SIZE)),0,PIXEL_SIZE,PIXEL_SIZE)
        end
    end

    love.graphics.setCanvas(wall)
    love.graphics.setColor(1,1,1,1)
    love.graphics.setBlendMode('multiply','premultiplied')
    love.graphics.draw(light,0,0)

    love.graphics.setCanvas(screen)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(1,1,1,1)
    love.graphics.draw(background)
    love.graphics.draw(wall)

    love.graphics.setBlendMode('replace')
    love.graphics.setCanvas(screenglitch)
    love.graphics.draw(screen)

    local p = clamp(lerp(1,0.8,((player_y-(END_Y-100))/100)),0.8,1)
    if love.math.random() > p*p then
        love.graphics.setCanvas{screenglitch, stencil=true}
        love.graphics.stencil(glitchStencil, "replace", 1)
        love.graphics.setStencilTest("greater", 0)
        love.graphics.draw(screen,PIXEL_SIZE*math.floor(love.math.random(4,8)),0)
        love.graphics.setStencilTest()
    
        love.graphics.setCanvas(screen)
        love.graphics.draw(screenglitch)
    
        love.graphics.setCanvas{screenglitch, stencil=true}
        love.graphics.stencil(glitchStencil, "replace", 1)
        love.graphics.setStencilTest("greater", 0)
        love.graphics.draw(screen,PIXEL_SIZE*math.floor(love.math.random(4,8)),0)
        love.graphics.setStencilTest()
    end

end