end_message_timer = 0

function drawEndMessages(dt)
    if end_message_timer < 1 then
        MESSAGE_POSITION = SCREEN_HEIGHT/2 - 27
        love.graphics.setBlendMode('alpha')
        love.graphics.setColor(0,0,0,1)
        love.graphics.rectangle('fill',0,0,SCREEN_WIDTH,SCREEN_HEIGHT)
        love.graphics.setColor(1,1,1,1)
        love.graphics.print("      - NO SIGNAL -", font, PIXEL_SIZE*2+2, MESSAGE_POSITION)
    end
    end_message_timer = end_message_timer + dt
    if end_message_timer > 2 then
        end_message_timer = end_message_timer - 2
    end
end

function drawMessages()
    MESSAGE_HEIGHT = 100
    MESSAGE_POSITION = SCREEN_HEIGHT - MESSAGE_HEIGHT
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle('fill',0,MESSAGE_POSITION,SCREEN_WIDTH,MESSAGE_HEIGHT)
    love.graphics.setColor(1,1,1,1)
    love.graphics.print("IF YOU READ ME\nPLEASE RESPOND", font, PIXEL_SIZE*2, MESSAGE_POSITION)
end