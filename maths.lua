function clamp(x,min,max)
    if x < min then
        return min
    end
    if x > max then
        return max
    end
    return x
end

function lerp(a,b,t)
    return a + t * (b - a)
end