love = require "love"

moonshine = require('lib/moonshine')

require("maths")
require("audio")
require("wall")
require("messages")

function love.load()
    SCREEN_WIDTH = love.graphics.getWidth()
    SCREEN_HEIGHT = love.graphics.getHeight()
    love.graphics.setDefaultFilter("nearest", "nearest")
    loadSounds()
    sounds:playBGM()
    initWall()
    shader = moonshine(moonshine.effects.chromasep).chain(moonshine.effects.scanlines).chain(moonshine.effects.crt)
    screen = love.graphics.newCanvas()
    screenglitch = love.graphics.newCanvas()
    light = love.graphics.newCanvas()
    wall = love.graphics.newCanvas()
    background = love.graphics.newCanvas()
    backgroundlight = love.graphics.newCanvas()
    font = love.graphics.newFont('resources/font/PixelEmulator-xq08.ttf',7*PIXEL_SIZE*1.47/2,'mono')
end

y = 1
ladder_y = 1


SPEED = 5*1.35

fdt = 0

game_end = false

function love.update(dt)
    fdt = dt
    local speed = SPEED
    -- if love.keyboard.isDown('lshift') then
    --     speed = speed * 50
    -- end
    y = lerp(y,ladder_y,0.9*dt)
    if love.keyboard.isDown('down') then
        y = y + dt*speed
    end
    if love.keyboard.isDown('up') then
        y = y - dt*speed
    end
    if y < 1 then
        y = 1
    end
    if y > END_Y then
        game_end = true
        shader.chromasep.radius = 0
        sounds:mixVolumes(-1325)
    end
    if not game_end then
        if math.abs(ladder_y - y) > 3 then
            if y > ladder_y then
                ladder_y = y + 3
            else
                ladder_y = y - 3
            end
    
            sounds:playLadder()
        end
        sounds:mixVolumes(y)
        shader.crt.distortionFactor = {1.06, 1.065}
        shader.scanlines.width = PIXEL_SIZE/4
        shader.scanlines.opacity = 0.5
        shader.chromasep.angle = 0
        shader.chromasep.radius = 0
        if y > ROBOT_Y then
            local p = clamp(lerp(1,0.8,((y-(END_Y-100))/100)),0.8,1)
            if love.math.random() > p then
                shader.chromasep.radius = 10
            end
        end
    end
end

function love.draw()
    love.graphics.setCanvas(screen)
    love.graphics.clear(0,0,0,1)
    love.graphics.setCanvas(screenglitch)
    love.graphics.clear(0,0,0,1)
    if not game_end then
        drawWall(y,fdt)
    else
        drawEndMessages(fdt)
    end
    love.graphics.setCanvas()
    love.graphics.clear(0,0,0,1)
    love.graphics.setBlendMode('alpha')
    shader(function ()
        love.graphics.draw(screenglitch,0,0)
    end)
    --sounds:displayVolumes()
    --love.graphics.print("y:"..y,0,0)
end