require("maths")

ladder_volume = 0.5

sounds = {}

function loadSounds()
    sounds.bgms = {}
    sounds.bgms[0] = love.audio.newSource("resources/audio/stage-surface1.ogg", "static")
    sounds.bgms[0]:setLooping(true)
    sounds.bgms[0]:setVolume(0)
    sounds.bgms[1] = love.audio.newSource("resources/audio/stage-surface2.ogg", "static")
    sounds.bgms[1]:setLooping(true)
    sounds.bgms[1]:setVolume(0)
    sounds.bgms[2] = love.audio.newSource("resources/audio/stage1-1.ogg", "static")
    sounds.bgms[2]:setLooping(true)
    sounds.bgms[2]:setVolume(0)
    sounds.bgms[3] = love.audio.newSource("resources/audio/stage1-3.ogg", "static")
    sounds.bgms[3]:setLooping(true)
    sounds.bgms[3]:setVolume(0)
    sounds.bgms[4] = love.audio.newSource("resources/audio/stage1-5.ogg", "static")
    sounds.bgms[4]:setLooping(true)
    sounds.bgms[4]:setVolume(0)
    sounds.bgms[5] = love.audio.newSource("resources/audio/stage-blood.ogg", "static")
    sounds.bgms[5]:setLooping(true)
    sounds.bgms[5]:setVolume(0)
    sounds.bgms[6] = love.audio.newSource("resources/audio/stage-earth.ogg", "static")
    sounds.bgms[6]:setLooping(true)
    sounds.bgms[6]:setVolume(0)
    sounds.bgms[7] = love.audio.newSource("resources/audio/stage-metal.ogg", "static")
    sounds.bgms[7]:setLooping(true)
    sounds.bgms[7]:setVolume(0)
    sounds.bgms[8] = love.audio.newSource("resources/audio/stage-machine.ogg", "static")
    sounds.bgms[8]:setLooping(true)
    sounds.bgms[8]:setVolume(0)
    sounds.bgms[9] = love.audio.newSource("resources/audio/stage-tonal.ogg", "static")
    sounds.bgms[9]:setLooping(true)
    sounds.bgms[9]:setVolume(0)
    
    sounds.ladders = {}
    sounds.ladders[0] = love.audio.newSource("resources/audio/lader1-fullvrb.wav", "static")
    sounds.ladders[0]:setVolume(ladder_volume)
    sounds.ladders[1] = love.audio.newSource("resources/audio/lader2-fullvrb.wav", "static")
    sounds.ladders[1]:setVolume(ladder_volume)
    sounds.ladders[2] = love.audio.newSource("resources/audio/lader3-fullvrb.wav", "static")
    sounds.ladders[2]:setVolume(ladder_volume)
    sounds.ladders[3] = love.audio.newSource("resources/audio/lader4-fullvrb.wav", "static")
    sounds.ladders[3]:setVolume(ladder_volume)
    sounds.laddersdry = {}
    sounds.laddersdry[0] = love.audio.newSource("resources/audio/lader1-dry.wav", "static")
    sounds.laddersdry[0]:setVolume(ladder_volume)
    sounds.laddersdry[1] = love.audio.newSource("resources/audio/lader2-dry.wav", "static")
    sounds.laddersdry[1]:setVolume(ladder_volume)
    sounds.laddersdry[2] = love.audio.newSource("resources/audio/lader3-dry.wav", "static")
    sounds.laddersdry[2]:setVolume(ladder_volume)
    sounds.laddersdry[3] = love.audio.newSource("resources/audio/lader4-dry.wav", "static")
    sounds.laddersdry[3]:setVolume(ladder_volume)
end

function sounds:playBGM()
    self.bgms[0]:play()
    self.bgms[1]:play()
    self.bgms[2]:play()
    self.bgms[3]:play()
    self.bgms[4]:play()
    self.bgms[5]:play()
    self.bgms[6]:play()
    self.bgms[7]:play()
    self.bgms[8]:play()
    self.bgms[9]:play()
end

function sounds:setLadderReverb(wet)
    local wet_volume = ladder_volume * wet
    self.ladders[0]:setVolume(wet_volume)
    self.ladders[1]:setVolume(wet_volume)
    self.ladders[2]:setVolume(wet_volume)
    self.ladders[3]:setVolume(wet_volume)

    local dry_volume = ladder_volume * (1-wet)
    self.laddersdry[0]:setVolume(dry_volume)
    self.laddersdry[1]:setVolume(dry_volume)
    self.laddersdry[2]:setVolume(dry_volume)
    self.laddersdry[3]:setVolume(dry_volume)
end

function sounds:mixVolumes(y)
    if y > 300 then
        self:setLadderReverb(1)
    else
        self:setLadderReverb(clamp(y/300,0,1))
    end
    self.bgms[0]:setVolume(clamp((100-math.abs(y-100))/100,0,1))
    self.bgms[1]:setVolume(clamp((100-math.abs(y-200))/100,0,1))
    self.bgms[2]:setVolume(clamp((100-math.abs(y-300))/100,0,1))
    self.bgms[3]:setVolume(clamp((100-math.abs(y-400))/100,0,1))
    self.bgms[4]:setVolume(clamp((100-math.abs(y-500))/100,0,1))
    self.bgms[5]:setVolume(clamp((100-math.abs(y-700))/100,0,1))
    self.bgms[6]:setVolume(clamp((100-math.abs(y-800))/100,0,1))
    self.bgms[7]:setVolume(clamp((100-math.abs(y-900))/100,0,1))
    self.bgms[8]:setVolume(clamp((100-math.abs(y-1000))/100,0,1))
    self.bgms[9]:setVolume(clamp((100-math.abs(y-1100))/100,0,1))
end

function sounds:displayVolumes()
    for i = 0, 9, 1 do
        love.graphics.print("stage-"..i..":"..self.bgms[i]:getVolume(),0,16+i*16)
    end
end

previous_ladder = 1

function sounds:playLadder()
    previous_ladder = (previous_ladder + 1) % 4
    self.ladders[previous_ladder]:stop()
    self.ladders[previous_ladder]:play()
    self.laddersdry[previous_ladder]:stop()
    self.laddersdry[previous_ladder]:play()
end

